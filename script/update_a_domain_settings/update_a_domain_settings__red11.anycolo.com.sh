#!/usr/bin/env bash

SERVER=192.168.168.220
ROOT_PASSWD="Heisable12389!"

# no_ssl
#sshpass -p $ROOT_PASSWD scp ../../__server/etc/nginx/sites-available/red11.anycolo.com__without_ssl root@$SERVER:/etc/nginx/sites-available/red11.anycolo.com
#sshpass -p $ROOT_PASSWD scp ../../__server/etc/nginx/sites-available/red11.anycolo.com__without_ssl root@$SERVER:/etc/nginx/sites-enabled/red11.anycolo.com

# with_ssl
sshpass -p $ROOT_PASSWD scp ../../__server/etc/nginx/sites-available/red11.anycolo.com root@$SERVER:/etc/nginx/sites-available/red11.anycolo.com
sshpass -p $ROOT_PASSWD scp ../../__server/etc/nginx/sites-available/red11.anycolo.com root@$SERVER:/etc/nginx/sites-enabled/red11.anycolo.com
