#!/usr/bin/env bash

SERVER=192.168.168.220
ROOT_PASSWD="Heisable12389!"

# backup everything under /etc/init.d
sshpass -p $ROOT_PASSWD scp -r root@$SERVER:/etc/init.d/* ../../__server/etc/init.d/

# backup everything under /etc/nginx
sshpass -p $ROOT_PASSWD scp -r root@$SERVER:/etc/nginx/* ../../__server/etc/nginx/

# backup everything under /etc/php.d
sshpass -p $ROOT_PASSWD scp -r root@$SERVER:/etc/php.d/* ../../__server/etc/php.d/

# backup everything under /etc/php-fpm.d
sshpass -p $ROOT_PASSWD scp -r root@$SERVER:/etc/php-fpm.d/* ../../__server/etc/php-fpm.d/

# backup /etc/php.ini
sshpass -p $ROOT_PASSWD scp -r root@$SERVER:/etc/php.ini ../../__server/etc/

# backup /etc/php-fpm.conf
sshpass -p $ROOT_PASSWD scp -r root@$SERVER:/etc/php-fpm.conf ../../__server/etc/