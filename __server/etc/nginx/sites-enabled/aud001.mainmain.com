server {
    listen 80;
    server_name aud001.mainmain.com;
    access_log /nfs4/aud001mainmaincom/logs/access.log;
    error_log /nfs4/aud001mainmaincom/logs/error.log;
    root /nfs4/aud001mainmaincom/www;
        client_max_body_size    16M;

    error_page  404              /404.html;
    location = /404.html {
        root /nfs4/aud001mainmaincom/www;
    }


    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /nfs4/aud001mainmaincom/www;
    }

    location / {
        root /nfs4/aud001mainmaincom/www;
        index  index.html index.htm index.php;
    }

    location ~ \.php$ {
        root /nfs4/aud001mainmaincom/www;
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /nfs4/aud001mainmaincom/www$fastcgi_script_name;
    }

    location ~ /\.ht {
        deny  all;
    }
}

