server{
    listen 80;
    listen 443 ssl;

    ssl on;
    ssl_certificate      /nfs4/vid01xo11com/ssls/bundle_all.crt;
    ssl_certificate_key  /nfs4/vid01xo11com/ssls/vid01.xo11.com.key;
    
    server_name vid01.xo11.com;
    
    add_header Access-Control-Allow-Origin *;
    
    access_log /nfs4/vid01xo11com/logs/access.log;
    error_log /nfs4/vid01xo11com/logs/error.log;
    
    root /nfs4/vid01xo11com/www;
        client_max_body_size    16M;
    error_page  404              /404.html;
    location = /404.html {
        root /nfs4/vid01xo11com/www;
    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /nfs4/vid01xo11com/www;
    }
    
    location / {
        root /nfs4/vid01xo11com/www;
        index  index.html index.htm index.php;
        mp4;
        mp4_buffer_size       1m;
        mp4_max_buffer_size   5m;
    }
    
    location /20140321080837/ {
                root /nfs4/vid01xo11com/www;
        secure_link $arg_md5,$arg_expires;
        ##secure_link_md5 "$secure_link_expires$uri$remote_addr secret";
        secure_link_md5 "$secure_link_expires$uri secret";
        if ($secure_link = "") {
                return 403;
        }
        if ($secure_link = "0") {
                return 410;
        }
    }
    
    location ~ \.php$ {
        root /nfs4/vid01xo11com/www;
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /nfs4/vid01xo11com/www$fastcgi_script_name;
    }
    
    location ~ /\.ht {
        deny  all;
    }
}
