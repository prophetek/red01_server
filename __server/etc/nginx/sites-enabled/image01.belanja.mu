server {
    listen 80;
    server_name image01.belanja.mu;
    access_log /nfs2/image01belanjamu/logs/access.log;
    error_log /nfs2/image01belanjamu/logs/error.log;
    root /nfs2/image01belanjamu/www;
        client_max_body_size    16M;

    error_page  404              /404.html;
    location = /404.html {
        root /nfs2/image01belanjamu/www;
    }


    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /nfs2/image01belanjamu/www;
    }

    location / {
        root /nfs2/image01belanjamu/www;
        index  index.html index.htm index.php;
    }

    location ~ \.php$ {
        root /nfs2/image01belanjamu/www;
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /nfs2/image01belanjamu/www$fastcgi_script_name;
    }

    location ~ /\.ht {
        deny  all;
    }
}
