server {
    listen 80;
    server_name coach.xosz.com;
    access_log /home/coachxoszcom/logs/access.log;
    error_log /home/coachxoszcom/logs/error.log;
    root /home/coachxoszcom/www;
        client_max_body_size    16M;

    error_page  404              /404.html;
    location = /404.html {
        root /home/coachxoszcom/www;
    }


    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /home/coachxoszcom/www;
    }

    location / {
        root /home/coachxoszcom/www;
        index  index.html index.htm index.php;
    }

    location ~ \.php$ {
        root /home/coachxoszcom/www;
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /home/coachxoszcom/www$fastcgi_script_name;
    }

    location ~ /\.ht {
        deny  all;
    }
}

