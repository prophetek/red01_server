server {
    listen 80;
    server_name avatar01.tokobesar.com;
    access_log /home/avatar01tokobesarcom/logs/access.log;
    error_log /home/avatar01tokobesarcom/logs/error.log;
    root /home/avatar01tokobesarcom/www;
        client_max_body_size    16M;

    error_page  404              /404.html;
    location = /404.html {
        root /home/avatar01tokobesarcom/www;
    }


    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /home/avatar01tokobesarcom/www;
    }

    location / {
        root /home/avatar01tokobesarcom/www;
        index  index.html index.htm index.php;
    }

    location ~ \.php$ {
        root /home/avatar01tokobesarcom/www;
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /home/avatar01tokobesarcom/www$fastcgi_script_name;
    }

    location ~ /\.ht {
        deny  all;
    }
}
