server {
    listen 80;
    
    listen 443 ssl;
    
    ssl on;
    ssl_certificate      /home/red11anycolocom/ssls/bundle_all.crt;
    ssl_certificate_key  /home/red11anycolocom/ssls/red11.anycolo.com.key;
    
    server_name red11.anycolo.com;
    
    access_log off;
    error_log /home/red11anycolocom/logs/error.log;
    root /home/red11anycolocom/www;
        client_max_body_size    4096M;
    error_page  404              /404.html;
    location = /404.html {
        root /home/red11anycolocom/www;
    }


    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /home/red11anycolocom/www;
    }

    location / {
        root /home/red11anycolocom/www;
        index  index.html index.htm index.php;
        client_max_body_size    4096M;
if (!-e $request_filename) {
rewrite ^/(.*)$ /index.php?/$1 last;
}
    }

    location ~ \.php$ {
        root /home/red11anycolocom/www;
        include /etc/nginx/fastcgi_params;
fastcgi_read_timeout 240;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /home/red11anycolocom/www$fastcgi_script_name;
    }

# serve static files directly
location ~* ^.+.(jpg|jpeg|gif|css|png|ico)$ {
        expires 30d;
}

    location ~ /\.ht {
        deny  all;
    }
}
