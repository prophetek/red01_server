server {
    listen 80;
    server_name image01.koranindonesia.com;
    access_log /home/image01koranindonesiacom/logs/access.log;
    error_log /home/image01koranindonesiacom/logs/error.log;
    root /home/image01koranindonesiacom/www;
        client_max_body_size    16M;

    error_page  404              /404.html;
    location = /404.html {
        root /home/image01koranindonesiacom/www;
    }


    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /home/image01koranindonesiacom/www;
    }

    location / {
        root /home/image01koranindonesiacom/www;
        index  index.html index.htm index.php;
    }

    location ~ \.php$ {
        root /home/image01koranindonesiacom/www;
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /home/image01koranindonesiacom/www$fastcgi_script_name;
    }

    location ~ /\.ht {
        deny  all;
    }
}

