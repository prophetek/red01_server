server {
    listen 80;
    server_name red01.anycolo.com www1-red01.anycolo.com;
    access_log /home/red01anycolocom/logs/access.log;
    error_log /home/red01anycolocom/logs/error.log;
    root /home/red01anycolocom/www;
	client_max_body_size	4096M;
    error_page  404              /404.html;
    location = /404.html {
        root /home/red01anycolocom/www;
    }


    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /home/red01anycolocom/www;
    }

    location / {
        root /home/red01anycolocom/www;
        index  index.html index.htm index.php;
	client_max_body_size	4096M;
if (!-e $request_filename) {
rewrite ^/(.*)$ /index.php?/$1 last;
}
    }

    location ~ \.php$ {
        root /home/red01anycolocom/www;
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /home/red01anycolocom/www$fastcgi_script_name;
    }

# serve static files directly
location ~* ^.+.(jpg|jpeg|gif|css|png|ico)$ {
	expires 30d;
}

    location ~ /\.ht {
        deny  all;
    }
}
