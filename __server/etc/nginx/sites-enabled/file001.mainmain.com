server {
    listen 80;
    server_name file001.mainmain.com;
    add_header Access-Control-Allow-Origin *;
    access_log /nfs4/file001mainmaincom/logs/access.log;
    error_log /nfs4/file001mainmaincom/logs/error.log;
    root /nfs4/file001mainmaincom/www;
        client_max_body_size    16M;

    error_page  404              /404.html;
    location = /404.html {
        root /nfs4/file001mainmaincom/www;
    }


    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /nfs4/file001mainmaincom/www;
    }

    location / {
        root /nfs4/file001mainmaincom/www;
        index  index.html index.htm index.php;
    }
    
    location /20140321081024/ {
		root /nfs4/file001mainmaincom/www;

        secure_link $arg_md5,$arg_expires;
        ##secure_link_md5 "$secure_link_expires$uri$remote_addr secret";
        secure_link_md5 "$secure_link_expires$uri secret";

        if ($secure_link = "") {
                return 403;
        }

        if ($secure_link = "0") {
                return 410;
        }
	}

    location ~ \.php$ {
        root /nfs4/file001mainmaincom/www;
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /nfs4/file001mainmaincom/www$fastcgi_script_name;
    }

    location ~ /\.ht {
        deny  all;
    }
}

