server {
    listen 80;
    server_name videopic01.babylongirl.com;
    access_log /nfs2/videopic01babylongirlcom/logs/access.log;
    error_log /nfs2/videopic01babylongirlcom/logs/error.log;
    root /nfs2/videopic01babylongirlcom/www;
    client_max_body_size    4096M;

    error_page  404              /404.html;
    location = /404.html {
        root /nfs2/videopic01babylongirlcom/www;
    }


    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /nfs2/videopic01babylongirlcom/www;
    }

    location / {
        root /nfs2/videopic01babylongirlcom/www;
        index  index.html index.htm index.php;
        client_max_body_size    4096M;
    }

    location ~ \.php$ {
        root /nfs2/videopic01babylongirlcom/www;
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /nfs2/videopic01babylongirlcom/www$fastcgi_script_name;
    }

    location ~ /\.ht {
        deny  all;
    }
}
