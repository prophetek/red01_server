server {
    listen 80;
    server_name images.dacupid.com;
    access_log /home/imagesdacupidcom/logs/access.log;
    error_log /home/imagesdacupidcom/logs/error.log;
    root /home/imagesdacupidcom/www;
        client_max_body_size    16M;

    error_page  404              /404.html;
    location = /404.html {
        root /home/imagesdacupidcom/www;
    }


    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /home/imagesdacupidcom/www;
    }

    location / {
        root /home/imagesdacupidcom/www;
        index  index.html index.htm index.php;
    }

    location ~ \.php$ {
        root /home/imagesdacupidcom/www;
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /home/imagesdacupidcom/www$fastcgi_script_name;
    }

    location ~ /\.ht {
        deny  all;
    }
}

