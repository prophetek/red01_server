server {
    listen 80;
    server_name list01.motor.mu;
    access_log /home/list01motormu/logs/access.log;
    error_log /home/list01motormu/logs/error.log;
    root /home/list01motormu/www;
	client_max_body_size	16M;

    error_page  404              /404.html;
    location = /404.html {
        root /home/list01motormu/www;
    }


    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /home/list01motormu/www;
    }

    location / {
        root /home/list01motormu/www;
        index  index.html index.htm index.php;
    }

    location ~ \.php$ {
        root /home/list01motormu/www;
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /home/list01motormu/www$fastcgi_script_name;
    }
        
    location ~ /\.ht {
        deny  all;
    }
}
