server {
    listen 80;
    server_name lib01.lagu.mu;
    access_log /home/lib01lagumu/logs/access.log;
    error_log /home/lib01lagumu/logs/error.log;
    root /home/lib01lagumu/www;
	client_max_body_size	16M;

    error_page  404              /404.html;
    location = /404.html {
        root /home/lib01lagumu/www;
    }


    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /home/lib01lagumu/www;
    }

    location / {
        root /home/lib01lagumu/www;
        index  index.html index.htm index.php;
    }

    location ~ \.php$ {
        root /home/lib01lagumu/www;
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /home/lib01lagumu/www$fastcgi_script_name;
    }
        
    location ~ /\.ht {
        deny  all;
    }
}
