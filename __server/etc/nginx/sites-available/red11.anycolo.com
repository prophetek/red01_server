server {
    listen 80;
    
    listen 443 ssl;
    
    ssl on;

    ssl_certificate           /etc/letsencrypt/live/red11.anycolo.com/fullchain.pem;
    ssl_certificate_key       /etc/letsencrypt/live/red11.anycolo.com/privkey.pem;

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA';
    ssl_session_timeout 1d;
    ssl_session_cache shared:SSL:50m;
    ssl_stapling on;
    ssl_stapling_verify on;
    add_header Strict-Transport-Security max-age=15768000;

    
    server_name red11.anycolo.com;
    
    access_log off;
    error_log /home/red11anycolocom/logs/error.log;
    root /home/red11anycolocom/www;
        client_max_body_size    4096M;
    error_page  404              /404.html;
    location = /404.html {
        root /home/red11anycolocom/www;
    }


    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /home/red11anycolocom/www;
    }

    location / {
        root /home/red11anycolocom/www;
        index  index.html index.htm index.php;
        client_max_body_size    4096M;
if (!-e $request_filename) {
rewrite ^/(.*)$ /index.php?/$1 last;
}
    }

    location ~ \.php$ {
        root /home/red11anycolocom/www;
        include /etc/nginx/fastcgi_params;
fastcgi_read_timeout 240;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /home/red11anycolocom/www$fastcgi_script_name;
    }

# serve static files directly
location ~* ^.+.(jpg|jpeg|gif|css|png|ico)$ {
        expires 30d;
}

    location ~ /\.ht {
        deny  all;
    }
}
