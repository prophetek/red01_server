server {
    listen 80;
    server_name img111.cityxguide.com;
    access_log /nfs2/img111cityxguidecom/logs/access.log;
    error_log /nfs2/img111cityxguidecom/logs/error.log;
    root /nfs2/img111cityxguidecom/www;
        client_max_body_size    16M;
    charset utf-8;
    location ^~ /.well-known/acme-challenge/ {
        # the usual settings
        allow all;
    }
    location ~ /\. { deny all; }
    location = /favicon.ico { }
   
    error_page  404              /404.html;
    location = /404.html {
        root /nfs2/img111cityxguidecom/www;
    }
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /nfs2/img111cityxguidecom/www;
    }
    location / {
        root /nfs2/img111cityxguidecom/www;
        index  index.html index.htm index.php;
    }
    location ~ \.php$ {
        root /nfs2/img111cityxguidecom/www;
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /nfs2/img111cityxguidecom/www$fastcgi_script_name;
    }
    location ~ /\.ht {
        deny  all;
    }
    location ~* ^.+\.(ogg|ogv|svg|svgz|eot|otf|woff|mp4|ttf|css|rss|atom|js|jpg|jpeg|gif|png|ico|zip|tgz|gz|rar|bz2|doc|xls|exe|ppt|tar|mid|midi|wav|bmp|rtf)$ {
        expires 360d;
        add_header Vary Accept-Encoding;
        log_not_found off;
        access_log off;
    }
}

server {
    listen 443;
    server_name img111.cityxguide.com;

    ssl_certificate           /etc/letsencrypt/live/img111.cityxguide.com/fullchain.pem;
    ssl_certificate_key       /etc/letsencrypt/live/img111.cityxguide.com/privkey.pem;

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA';
    ssl_session_timeout 1d;
    ssl_session_cache shared:SSL:50m;
    ssl_stapling on;
    ssl_stapling_verify on;
    add_header Strict-Transport-Security max-age=15768000;

    root /nfs2/img111cityxguidecom/www;

    index index.php;

    access_log /nfs2/img111cityxguidecom/logs/access.log;
    error_log /nfs2/img111cityxguidecom/logs/error.log;

    charset utf-8;

    location ^~ /.well-known/acme-challenge/ {
        # the usual settings
        allow all;
    }

    location ~ /\. { deny all; }
    location = /favicon.ico { }
    location = /robots.txt { }

    location ~ \.php$ {
        include         /etc/nginx/fastcgi_params;
        fastcgi_pass    127.0.0.1:9000;
        fastcgi_index   index.php;
        fastcgi_param   SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include         fastcgi_params;
    }

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~* ^.+\.(ogg|ogv|svg|svgz|eot|otf|woff|mp4|ttf|css|rss|atom|js|jpg|jpeg|gif|png|ico|zip|tgz|gz|rar|bz2|doc|xls|exe|ppt|tar|mid|midi|wav|bmp|rtf)$ {
        expires 30d;
        add_header Vary Accept-Encoding;
        log_not_found off;
        access_log off;
    }

}
