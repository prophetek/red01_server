server{
    listen 80;
    server_name log.prophetek.com;

    access_log /home/logprophetekcom/logs/access.log;
    error_log /home/logprophetekcom/logs/error.log;
    
    root /home/logprophetekcom/www;
        client_max_body_size    16M;

    error_page  404              /404.html;
    location = /404.html {
        root /home/logprophetekcom/www;
    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /home/logprophetekcom/www;
    }

    location / {
        root /home/logprophetekcom/www;
        index  index.html index.htm index.php;
        autoindex on;
        
        allow   192.168.168.0/24;
        allow   184.105.148.0/24;
        
        deny all;
    }

    location ~ /\.ht {
        deny  all;
    }
}

