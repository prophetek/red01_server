server {
	listen         80;
    server_name img01.xo11.com;
	return         301 https://img01.xo11.com$request_uri;
}

server {
    listen 443 ssl;

    ssl on;
    ssl_certificate      /home/img01xo11com/ssl/img01.xo11.com/bundle_all.crt;
    ssl_certificate_key  /home/img01xo11com/ssl/img01.xo11.com/img01.xo11.com.key;

    server_name img01.xo11.com;
    access_log /home/img01xo11com/logs/access.log;
    error_log /home/img01xo11com/logs/error.log;
    root /home/img01xo11com/www;
        client_max_body_size    16M;

    location = /404.html {
        root /home/img01xo11com/www;
    }


    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /home/img01xo11com/www;
    }

    location / {
        root /home/img01xo11com/www;
        index  index.html index.htm index.php;
    }

    # enforce NO www
    if ($host ~* ^www\.(.*))
    {
        set $host_without_www $1;
        rewrite ^/(.*)$ $scheme://$host_without_www/$1 permanent;
    }
 
    # unless the request is for a valid file, send to bootstrap
    if (!-e $request_filename)
    {
        rewrite ^(.+)$ /index.php?q=$1 last;
    }
 
    # catch all
    error_page 404 /index.php;

    location ~ \.php$ {
        root /home/img01xo11com/www;
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /home/img01xo11com/www$fastcgi_script_name;
    }

    location ~ /\.ht {
        deny  all;
    }
}
