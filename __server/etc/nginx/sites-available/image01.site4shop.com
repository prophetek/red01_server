server {
    listen 80;
    server_name image01.site4shop.com;
    access_log /nfs2/image01site4shopcom/logs/access.log;
    error_log /nfs2/image01site4shopcom/logs/error.log;
    root /nfs2/image01site4shopcom/www;
        client_max_body_size    16M;
    charset utf-8;
    location ^~ /.well-known/acme-challenge/ {
        # the usual settings
        allow all;
    }
    location ~ /\. { deny all; }
    location = /favicon.ico { }
  
    error_page  404              /404.html;
    location = /404.html {
        root /nfs2/image01site4shopcom/www;
    }
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /nfs2/image01site4shopcom/www;
    }
    location / {
        root /nfs2/image01site4shopcom/www;
        index  index.html index.htm index.php;
    }
    location ~ \.php$ {
        root /nfs2/image01site4shopcom/www;
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /nfs2/image01site4shopcom/www$fastcgi_script_name;
    }
    location ~ /\.ht {
        deny  all;
    }
    location ~* ^.+\.(ogg|ogv|svg|svgz|eot|otf|woff|mp4|ttf|css|rss|atom|js|jpg|jpeg|gif|png|ico|zip|tgz|gz|rar|bz2|doc|xls|exe|ppt|tar|mid|midi|wav|bmp|rtf)$ {
        expires 360d;
        add_header Vary Accept-Encoding;
        log_not_found off;
        access_log off;
    }
}
