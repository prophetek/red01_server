server {
    listen 80;
    server_name albumart01.lagu.mu;
    access_log /home/albumart01lagumu/logs/access.log;
    error_log /home/albumart01lagumu/logs/error.log;
    root /home/albumart01lagumu/www;
        client_max_body_size    16M;

    error_page  404              /404.html;
    location = /404.html {
        root /home/albumart01lagumu/www;
    }


    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /home/albumart01lagumu/www;
    }

    location / {
        root /home/albumart01lagumu/www;
        index  index.html index.htm index.php;
    }

    location ~ \.php$ {
        root /home/albumart01lagumu/www;
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /home/albumart01lagumu/www$fastcgi_script_name;
    }

    location ~ /\.ht {
        deny  all;
    }
}
