server {
    listen 80;
    server_name video01.babylongirl.com;
    access_log /nfs2/video01babylongirlcom/logs/access.log;
    error_log /nfs2/video01babylongirlcom/logs/error.log;
    root /nfs2/video01babylongirlcom/www;
    client_max_body_size    4096M;

    error_page  404              /404.html;
    location = /404.html {
        root /nfs2/video01babylongirlcom/www;
    }


    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /nfs2/video01babylongirlcom/www;
    }

    location / {
        root /nfs2/video01babylongirlcom/www;
        index  index.html index.htm index.php;
        proxy_read_timeout 180; 
        client_max_body_size    4096M;
    }

    location ~ \.php$ {
        root /nfs2/video01babylongirlcom/www;
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /nfs2/video01babylongirlcom/www$fastcgi_script_name;
    }

    location ~ /\.ht {
        deny  all;
    }

	location ~* \.(ttf|srt)$ {
    	add_header 'Access-Control-Allow-Origin' "*";
    	expires 1M;
    	access_log off;
    	add_header Cache-Control "public";
  	}
}
