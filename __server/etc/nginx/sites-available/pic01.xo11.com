server {
    listen 80;
    listen 443 ssl;

    ssl on;
    ssl_certificate      /home/pic01xo11com/ssl/pic01.xo11.com/bundle_all.crt;
    ssl_certificate_key  /home/pic01xo11com/ssl/pic01.xo11.com/pic01.xo11.com.key;
    ssl_session_cache none;

    server_name pic01.xo11.com;
    access_log /home/pic01xo11com/logs/access.log;
    error_log /home/pic01xo11com/logs/error.log;
    root /home/pic01xo11com/www;
        client_max_body_size    16M;

    error_page  404              /404.html;
    location = /404.html {
        root /home/pic01xo11com/www;
    }


    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /home/pic01xo11com/www;
    }

    location / {
        root /home/pic01xo11com/www;
        index  index.html index.htm index.php;
    }

    location ~ \.php$ {
        root /home/pic01xo11com/www;
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /home/pic01xo11com/www$fastcgi_script_name;
    }

    location ~ /\.ht {
        deny  all;
    }
}
