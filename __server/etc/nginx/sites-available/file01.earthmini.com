server {
    listen 80;
    server_name file01.earthmini.com 192.168.168.220;
    access_log /home/file01earthminicom/logs/access.log;
    error_log /home/file01earthminicom/logs/error.log;
    root /home/file01earthminicom/www;
	client_max_body_size	16M;
    error_page  404              /404.html;
    location = /404.html {
        root /home/file01earthminicom/www;
    }


    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /home/file01earthminicom/www;
    }

    location / {
        root /home/file01earthminicom/www;
        index  index.html index.htm index.php;
    }

    location ~ \.php$ {
        root /home/file01earthminicom/www;
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /home/file01earthminicom/www$fastcgi_script_name;
    }

    location ~ /\.ht {
        deny  all;
    }
}
