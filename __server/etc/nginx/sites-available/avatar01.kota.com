server {
    listen 80;
    listen 443 ssl;
    
    ssl on;
    ssl_certificate      /nfs2/avatar01kotacom/ssls/bundle_all.crt;
    ssl_certificate_key  /nfs2/avatar01kotacom/ssls/avatar01.kota.com.key;

    server_name avatar01.kota.com;

    access_log /nfs2/avatar01kotacom/logs/access.log;
    error_log /nfs2/avatar01kotacom/logs/error.log;
    root /nfs2/avatar01kotacom/www;
        client_max_body_size    16M;


    error_page  404              /404.html;
    location = /404.html {
        root /nfs2/avatar01kotacom/www;
    }


    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /nfs2/avatar01kotacom/www;
    }

    location / {
        root /nfs2/avatar01kotacom/www;
        index  index.html index.htm index.php;
    }

    location ~ \.php$ {
        root /nfs2/avatar01kotacom/www;
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  /nfs2/avatar01kotacom/www$fastcgi_script_name;
    }

    location ~ /\.ht {
        deny  all;
    }

    location ~* ^.+\.(ogg|ogv|svg|svgz|eot|otf|woff|mp4|ttf|css|rss|atom|js|jpg|jpeg|gif|png|ico|zip|tgz|gz|rar|bz2|doc|xls|exe|ppt|tar|mid|midi|wav|bmp|rtf)$ {
        expires 360d;
        add_header Vary Accept-Encoding;
        log_not_found off;
        access_log off;
    }
}
