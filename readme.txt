1. Before installation

[root@red01 init.d]# nginx -v
nginx version: nginx/1.4.1

2. Docs reference

http://wiki.prophetek.com/display/SER/black111+-+installing+nginx+virtual+host

3. 
[root@red01 Downloads]# pwd
/home/wilhan/Downloads
[root@red01 Downloads]# wget http://nginx.org/download/nginx-1.8.0.tar.gz
--2015-08-03 10:05:03--  http://nginx.org/download/nginx-1.8.0.tar.gz
Resolving nginx.org... 206.251.255.63, 2606:7100:1:69::3f
Connecting to nginx.org|206.251.255.63|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 832104 (813K) [application/octet-stream]
Saving to: “nginx-1.8.0.tar.gz”

100%[================================================================================================================================================================================>] 832,104     3.93M/s   in 0.2s    

2015-08-03 10:05:04 (3.93 MB/s) - “nginx-1.8.0.tar.gz” saved [832104/832104]

4. 
[root@red01 Downloads]# wget ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.36.tar.gz
--2015-08-03 10:08:50--  ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.36.tar.gz
           => “pcre-8.36.tar.gz”
Resolving ftp.csx.cam.ac.uk... 131.111.8.115
Connecting to ftp.csx.cam.ac.uk|131.111.8.115|:21... connected.
Logging in as anonymous ... Logged in!
==> SYST ... done.    ==> PWD ... done.
==> TYPE I ... done.  ==> CWD (1) /pub/software/programming/pcre ... done.
==> SIZE pcre-8.36.tar.gz ... 2009464
==> PASV ... done.    ==> RETR pcre-8.36.tar.gz ... done.
Length: 2009464 (1.9M) (unauthoritative)

100%[================================================================================================================================================================================>] 2,009,464   1.18M/s   in 1.6s    

2015-08-03 10:08:54 (1.18 MB/s) - “pcre-8.36.tar.gz” saved [2009464]

5.
[root@red01 Downloads]# wget http://zlib.net/zlib-1.2.8.tar.gz
--2015-08-03 10:09:27--  http://zlib.net/zlib-1.2.8.tar.gz
Resolving zlib.net... 69.73.132.10
Connecting to zlib.net|69.73.132.10|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 571091 (558K) [application/x-gzip]
Saving to: “zlib-1.2.8.tar.gz”

100%[================================================================================================================================================================================>] 571,091     1.24M/s   in 0.4s    

2015-08-03 10:09:27 (1.24 MB/s) - “zlib-1.2.8.tar.gz” saved [571091/571091]



6.
[root@red01 Downloads]# wget https://www.openssl.org/source/openssl-1.0.2d.tar.gz
--2015-08-03 10:10:35--  https://www.openssl.org/source/openssl-1.0.2d.tar.gz
Resolving www.openssl.org... 194.97.150.234, 2001:608:c00:180::1:ea
Connecting to www.openssl.org|194.97.150.234|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 5295447 (5.0M) [application/x-gzip]
Saving to: “openssl-1.0.2d.tar.gz”

100%[================================================================================================================================================================================>] 5,295,447   2.75M/s   in 1.8s    

2015-08-03 10:10:38 (2.75 MB/s) - “openssl-1.0.2d.tar.gz” saved [5295447/5295447]

7.
[root@red01 nginx-1.8.0]# pwd
/home/wilhan/Downloads/nginx-1.8.0

[root@red01 nginx-1.8.0]# service nginx stop
Stopping nginx:                                            [  OK  ]

[root@red01 nginx-1.8.0]# ps ax | grep -i nginx
21398 pts/0    R+     0:00 grep -i nginx

8.
./configure --user=nginx --sbin-path=/usr/sbin/nginx --conf-path=/etc/nginx/nginx.conf --pid-path=/var/run/nginx.pid --with-pcre=/home/wilhan/Downloads/pcre-8.36 --with-http_mp4_module --with-zlib=/home/wilhan/Downloads/zlib-1.2.8 --with-http_ssl_module --with-openssl=/home/wilhan/Downloads/openssl-1.0.2d

[root@red01 nginx-1.8.0]# ./configure --user=nginx --sbin-path=/usr/sbin/nginx --conf-path=/etc/nginx/nginx.conf --pid-path=/var/run/nginx.pid --with-pcre=/home/wilhan/Downloads/pcre-8.36 --with-http_secure_link_module --with-http_mp4_module --with-zlib=/home/wilhan/Downloads/zlib-1.2.8 --with-http_ssl_module --with-openssl=/home/wilhan/Downloads/openssl-1.0.2d
checking for OS
 + Linux 2.6.32-358.2.1.el6.x86_64 x86_64
checking for C compiler ... found
 + using GNU C compiler
 + gcc version: 4.4.7 20120313 (Red Hat 4.4.7-3) (GCC) 
checking for gcc -pipe switch ... found
checking for gcc builtin atomic operations ... found
checking for C99 variadic macros ... found
checking for gcc variadic macros ... found
checking for unistd.h ... found
checking for inttypes.h ... found
checking for limits.h ... found
checking for sys/filio.h ... not found
checking for sys/param.h ... found
checking for sys/mount.h ... found
checking for sys/statvfs.h ... found
checking for crypt.h ... found
checking for Linux specific features
checking for epoll ... found
checking for EPOLLRDHUP ... found
checking for O_PATH ... not found
checking for sendfile() ... found
checking for sendfile64() ... found
checking for sys/prctl.h ... found
checking for prctl(PR_SET_DUMPABLE) ... found
checking for sched_setaffinity() ... found
checking for crypt_r() ... found
checking for sys/vfs.h ... found
checking for poll() ... found
checking for /dev/poll ... not found
checking for kqueue ... not found
checking for crypt() ... not found
checking for crypt() in libcrypt ... found
checking for F_READAHEAD ... not found
checking for posix_fadvise() ... found
checking for O_DIRECT ... found
checking for F_NOCACHE ... not found
checking for directio() ... not found
checking for statfs() ... found
checking for statvfs() ... found
checking for dlopen() ... not found
checking for dlopen() in libdl ... found
checking for sched_yield() ... found
checking for SO_SETFIB ... not found
checking for SO_ACCEPTFILTER ... not found
checking for TCP_DEFER_ACCEPT ... found
checking for TCP_KEEPIDLE ... found
checking for TCP_FASTOPEN ... not found
checking for TCP_INFO ... found
checking for accept4() ... found
checking for eventfd() ... found
checking for int size ... 4 bytes
checking for long size ... 8 bytes
checking for long long size ... 8 bytes
checking for void * size ... 8 bytes
checking for uint64_t ... found
checking for sig_atomic_t ... found
checking for sig_atomic_t size ... 4 bytes
checking for socklen_t ... found
checking for in_addr_t ... found
checking for in_port_t ... found
checking for rlim_t ... found
checking for uintptr_t ... uintptr_t found
checking for system byte ordering ... little endian
checking for size_t size ... 8 bytes
checking for off_t size ... 8 bytes
checking for time_t size ... 8 bytes
checking for setproctitle() ... not found
checking for pread() ... found
checking for pwrite() ... found
checking for sys_nerr ... found
checking for localtime_r() ... found
checking for posix_memalign() ... found
checking for memalign() ... found
checking for mmap(MAP_ANON|MAP_SHARED) ... found
checking for mmap("/dev/zero", MAP_SHARED) ... found
checking for System V shared memory ... found
checking for POSIX semaphores ... not found
checking for POSIX semaphores in libpthread ... found
checking for struct msghdr.msg_control ... found
checking for ioctl(FIONBIO) ... found
checking for struct tm.tm_gmtoff ... found
checking for struct dirent.d_namlen ... not found
checking for struct dirent.d_type ... found
checking for sysconf(_SC_NPROCESSORS_ONLN) ... found
checking for openat(), fstatat() ... found
checking for getaddrinfo() ... found
creating objs/Makefile

Configuration summary
  + using PCRE library: /home/wilhan/Downloads/pcre-8.36
  + using OpenSSL library: /home/wilhan/Downloads/openssl-1.0.2d
  + md5: using OpenSSL library
  + sha1: using OpenSSL library
  + using zlib library: /home/wilhan/Downloads/zlib-1.2.8

  nginx path prefix: "/usr/local/nginx"
  nginx binary file: "/usr/sbin/nginx"
  nginx configuration prefix: "/etc/nginx"
  nginx configuration file: "/etc/nginx/nginx.conf"
  nginx pid file: "/var/run/nginx.pid"
  nginx error log file: "/usr/local/nginx/logs/error.log"
  nginx http access log file: "/usr/local/nginx/logs/access.log"
  nginx http client request body temporary files: "client_body_temp"
  nginx http proxy temporary files: "proxy_temp"
  nginx http fastcgi temporary files: "fastcgi_temp"
  nginx http uwsgi temporary files: "uwsgi_temp"
  nginx http scgi temporary files: "scgi_temp"

  
9.
[root@red01 nginx-1.8.0]# make
make -f objs/Makefile
make[1]: Entering directory `/home/wilhan/Downloads/nginx-1.8.0'
cd /home/wilhan/Downloads/pcre-8.36 \
	&& if [ -f Makefile ]; then make distclean; fi \
	&& CC="cc" CFLAGS="-O2 -fomit-frame-pointer -pipe " \
	./configure --disable-shared 
make[2]: Entering directory `/home/wilhan/Downloads/pcre-8.36'
 rm -f pcretest pcregrep
test -z "pcre_chartables.c testsavedregex teststderr testtemp* testtry testNinput testtrygrep teststderrgrep testNinputgrep" || rm -f pcre_chartables.c testsavedregex teststderr testtemp* testtry testNinput testtrygrep teststderrgrep testNinputgrep
test -z "libpcre.la   libpcreposix.la libpcrecpp.la" || rm -f libpcre.la   libpcreposix.la libpcrecpp.la
rm -f ./so_locations
rm -rf .libs _libs
 rm -f pcrecpp_unittest pcre_scanner_unittest pcre_stringpiece_unittest
rm -f *.o
test -z "pcrecpp_unittest.log pcre_scanner_unittest.log pcre_stringpiece_unittest.log RunTest.log RunGrepTest.log" || rm -f pcrecpp_unittest.log pcre_scanner_unittest.log pcre_stringpiece_unittest.log RunTest.log RunGrepTest.log
test -z "pcrecpp_unittest.trs pcre_scanner_unittest.trs pcre_stringpiece_unittest.trs RunTest.trs RunGrepTest.trs" || rm -f pcrecpp_unittest.trs pcre_scanner_unittest.trs pcre_stringpiece_unittest.trs RunTest.trs RunGrepTest.trs
test -z "test-suite.log" || rm -f test-suite.log
rm -f *.lo
rm -f *.tab.c
test -z "libpcre.pc libpcre16.pc libpcre32.pc libpcreposix.pc libpcrecpp.pc pcre-config pcre.h pcre_stringpiece.h pcrecpparg.h" || rm -f libpcre.pc libpcre16.pc libpcre32.pc libpcreposix.pc libpcrecpp.pc pcre-config pcre.h pcre_stringpiece.h pcrecpparg.h
test . = "." || test -z "" || rm -f 
rm -f config.h stamp-h1
rm -f libtool config.lt
rm -f TAGS ID GTAGS GRTAGS GSYMS GPATH tags
rm -f cscope.out cscope.in.out cscope.po.out cscope.files
rm -f config.status config.cache config.log configure.lineno config.status.lineno
rm -rf ./.deps
rm -f Makefile
make[2]: Leaving directory `/home/wilhan/Downloads/pcre-8.36'




	objs/ngx_modules.o \
	-lpthread -lcrypt /home/wilhan/Downloads/pcre-8.36/.libs/libpcre.a /home/wilhan/Downloads/openssl-1.0.2d/.openssl/lib/libssl.a /home/wilhan/Downloads/openssl-1.0.2d/.openssl/lib/libcrypto.a -ldl /home/wilhan/Downloads/zlib-1.2.8/libz.a
make[1]: Leaving directory `/home/wilhan/Downloads/nginx-1.8.0'
make -f objs/Makefile manpage
make[1]: Entering directory `/home/wilhan/Downloads/nginx-1.8.0'
sed -e "s|%%PREFIX%%|/usr/local/nginx|" \
		-e "s|%%PID_PATH%%|/var/run/nginx.pid|" \
		-e "s|%%CONF_PATH%%|/etc/nginx/nginx.conf|" \
		-e "s|%%ERROR_LOG_PATH%%|/usr/local/nginx/logs/error.log|" \
		< man/nginx.8 > objs/nginx.8
make[1]: Leaving directory `/home/wilhan/Downloads/nginx-1.8.0'


10.

[root@red01 nginx-1.8.0]# make install
make -f objs/Makefile install
make[1]: Entering directory `/home/wilhan/Downloads/nginx-1.8.0'
test -d '/usr/local/nginx' || mkdir -p '/usr/local/nginx'
test -d '/usr/sbin' 		|| mkdir -p '/usr/sbin'
test ! -f '/usr/sbin/nginx' 		|| mv '/usr/sbin/nginx' 			'/usr/sbin/nginx.old'
cp objs/nginx '/usr/sbin/nginx'
test -d '/etc/nginx' 		|| mkdir -p '/etc/nginx'
cp conf/koi-win '/etc/nginx'
cp conf/koi-utf '/etc/nginx'
cp conf/win-utf '/etc/nginx'
test -f '/etc/nginx/mime.types' 		|| cp conf/mime.types '/etc/nginx'
cp conf/mime.types '/etc/nginx/mime.types.default'
test -f '/etc/nginx/fastcgi_params' 		|| cp conf/fastcgi_params '/etc/nginx'
cp conf/fastcgi_params 		'/etc/nginx/fastcgi_params.default'
test -f '/etc/nginx/fastcgi.conf' 		|| cp conf/fastcgi.conf '/etc/nginx'
cp conf/fastcgi.conf '/etc/nginx/fastcgi.conf.default'
test -f '/etc/nginx/uwsgi_params' 		|| cp conf/uwsgi_params '/etc/nginx'
cp conf/uwsgi_params 		'/etc/nginx/uwsgi_params.default'
test -f '/etc/nginx/scgi_params' 		|| cp conf/scgi_params '/etc/nginx'
cp conf/scgi_params 		'/etc/nginx/scgi_params.default'
test -f '/etc/nginx/nginx.conf' 		|| cp conf/nginx.conf '/etc/nginx/nginx.conf'
cp conf/nginx.conf '/etc/nginx/nginx.conf.default'
test -d '/var/run' 		|| mkdir -p '/var/run'
test -d '/usr/local/nginx/logs' || 		mkdir -p '/usr/local/nginx/logs'
test -d '/usr/local/nginx/html' 		|| cp -R html '/usr/local/nginx'
test -d '/usr/local/nginx/logs' || 		mkdir -p '/usr/local/nginx/logs'
make[1]: Leaving directory `/home/wilhan/Downloads/nginx-1.8.0'

11.
[root@red01 nginx-1.8.0]# service nginx configtest
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful

[root@red01 nginx-1.8.0]# nginx -v
nginx version: nginx/1.8.0

12.
[root@red01 nginx-1.8.0]# service nginx start
Starting nginx:                                            [  OK  ]
